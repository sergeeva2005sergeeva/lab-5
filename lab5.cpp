#include <iostream>
using namespace std;

//рандомная генерация массива 
void randomArray(int* arr, int size) {
    cout << "array:\n";
    for (int i = 0; i < size; i++) {
        arr[i] = rand() % 100; 
        cout << arr[i] << " ";
    }
    cout << endl;
}
//вводим массив с клавиатуры
void inputArray(int* arr, int size) {
    cout << "enter elements:\n";
    for (int i = 0; i < size; i++) {
        cin >> arr[i];
    }
}

// вывод массива на консоль
void printArray(int* arr, int size) {
    cout << "Array:\n";
    for (int i = 0; i < size; i++) {
        cout << arr[i] << " ";
    }
    cout << endl;
}

//ищет и удаляет четн. элементы на нечетн. позициях
void findAndRemove(int* arr, int size, int*& result, int& resultSize) {
    resultSize = 0;
    for (int i = 1; i < size; i += 2) {
        if (arr[i] % 2 == 0) {
            resultSize++;
        }
    }
    
    if (resultSize == 0) {
        result = nullptr;
        return;
    }
    
    result = new int[resultSize];
    int index = 0;
    for (int i = 1; i < size; i += 2) {
        if (arr[i] % 2 == 0) {
            result[index++] = arr[i];
            for (int j = i; j < size - 1; j++) {
                arr[j] = arr[j + 1];
            }
            size--;
            i--;
        }
    }
}

//сортирует массив по убыванию при делении на а
void sortArray(int* arr, int size, int a) {
    for (int i = 0; i < size - 1; i++) {
        for (int j = 0; j < size - i - 1; j++) {
            if (arr[j] % a < arr[j + 1] % a) {
                int temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
}

int main() {
    int size;
    cout << "Enter array dimension: ";
    cin >> size;

    int* arr = new int[size];
    int* result;
    int resultSize;

    int choice;
    cout << "Choose how to fill the array:\n";
    cout << "1. keyboard\n";
    cout << "2. random\n";
    cin >> choice;

    switch (choice) {
        case 1:
            inputArray(arr, size);
            break;
        case 2:
            randomArray(arr, size);
            break;
    }

    printArray(arr, size);

    findAndRemove(arr, size, result, resultSize);

    if (resultSize > 0) {
        cout << "Elements in odd positions with even values:\n";
        printArray(result, resultSize);
        delete[] result;
    } else {
        cout << "There are no elements that satisfy the condition.\n";
    }

    int a;
    cout << "Enter the value a to sort: ";
    cin >> a;

    sortArray(arr, size, a);

    cout << "array after sorting " << a << ":\n";
    printArray(arr, size);

    delete[] arr;

    return 0;
}